package com.web.nibelungen.sortag;

import com.web.nibelungen.sortag.yaml.Parameter;
import org.assertj.core.api.Assertions;

import java.util.ArrayList;

public class TestBase extends Assertions {

    /**
     *
     */
    protected static final String FIRSTTAG = "firsttag";

    /**
     *
     */
    protected static final String SECONDTAG = "secondtag";

    /**
     *
     */
    protected static final String RESSOURCE_FOLDER = "src/test/resources/";

    /**
     *
     */
    protected static final String FILE_SOURCE = "src/test/resources/testCopyFiles/file.txt";

    /**
     *
     */
    protected static final String FILE_DEST = "src/test/resources/testCopyFiles/copy.txt";

    /**
     *
     */
    protected static final String SRC_TEST_RESOURCES_COUNT_FILES = "src/test/resources/countFiles";

    /**
     *
     */
    protected static final String JPEG_EX = "jpegTest0.jpg";


    protected Parameter getParametres() {
        Parameter parameter = new Parameter();
        parameter.setExtensions(new ArrayList<>());
        parameter.getExtensions().add("jpg");
        parameter.getExtensions().add("jpeg");
        parameter.getExtensions().add("png");

        return parameter;
    }
}
