/**
 * Classe de Test pour FileUtils
 */
package com.web.nibelungen.sortag.utils;

import com.web.nibelungen.sortag.TestBase;
import com.web.nibelungen.sortag.exception.SorTagException;
import org.junit.Test;

import java.io.File;

/**
 * @author Mickaël
 *
 */
public class FileUtilsTest extends TestBase {

	/**
	 *
	 */
	@Test
	public void copyFile() {
		File sourceFile = new File(FILE_SOURCE);
		File destinationFile = new File(FILE_DEST);
		Boolean exist = false;
		Long fileSize = null;
		Boolean delete = null;

		if(destinationFile.exists()) {
			delete = destinationFile.delete();
		}

		assertThat(destinationFile.exists()).isFalse();
		assertThat(delete).isNull();

		try {
			FileUtils.copyFile(sourceFile, destinationFile);
		} catch (SorTagException e) {
			e.printStackTrace();
		}
		
		if(destinationFile.exists()) {
			exist = true;
			fileSize = destinationFile.length();
			delete = destinationFile.delete();
		}

		assertThat(delete).isTrue();
		assertThat(exist).isTrue();
		assertThat(fileSize).isEqualTo(Long.valueOf(sourceFile.length()));
	}
}
