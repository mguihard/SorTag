package com.web.nibelungen.sortag.api;

import com.web.nibelungen.sortag.TestBase;
import com.web.nibelungen.sortag.exception.SorTagException;
import com.web.nibelungen.sortag.yaml.Parameter;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.List;

public class SorTagApiTest extends TestBase {

    private SorTagApi sorTagApi;

    @Before
    public void setUp() throws Exception {
        this.sorTagApi = new SorTagApi(Parameter.build());
    }

    @Test
    public void folderApiCountFile() throws SorTagException {
        Long count = this.sorTagApi.folderApiCountFile(SRC_TEST_RESOURCES_COUNT_FILES);

        assertThat(count).isNotNull();
        assertThat(count).isEqualTo(4L);
    }

    @Test
    public void fileApiCopyFile() throws SorTagException {
        File sourceFile = new File(FILE_SOURCE);
        File destinationFile = new File(FILE_DEST);
        Boolean exist = false;
        Long fileSize = null;

        if(destinationFile.exists()) {
            destinationFile.delete();
        }


        this.sorTagApi.fileApiCopyFile(sourceFile, destinationFile);

        if(destinationFile.exists()) {
            exist = true;
            fileSize = destinationFile.length();
            destinationFile.delete();
        }

        assertThat(exist).isTrue();
        assertThat(fileSize).isEqualTo(Long.valueOf(sourceFile.length()));
    }

    @Test
    public void tagApiOfFile() throws SorTagException {
        File file = new File(RESSOURCE_FOLDER + JPEG_EX);
        List<String> allTagOfFile = this.sorTagApi.tagApiOfFile(file);

        assertThat(allTagOfFile).isNotNull();
        assertThat(allTagOfFile).isNotEmpty();
        assertThat(allTagOfFile.size()).isEqualTo(2);
        assertThat(allTagOfFile).containsExactly("firsttag", "secondtag");
    }

    @Test
    public void tagApiOfFolder() throws SorTagException {
        List<String> allTagOfFolder = this.sorTagApi.tagApiOfFolder(RESSOURCE_FOLDER);

        assertThat(allTagOfFolder).isNotNull();
        assertThat(allTagOfFolder.size()).isEqualTo(2);
        assertThat(allTagOfFolder).containsExactly("firsttag", "secondtag");
        assertThat(allTagOfFolder).isNotNull();
    }

    @Test
    public void getErrorMessage() throws SorTagException {
        String msg = this.sorTagApi.getErrorMessage("0x0", "test");

        assertThat(msg).isNotEmpty();
        assertThat(msg).isEqualTo("Impossible de charger le fichier YamlBase test");
    }
}