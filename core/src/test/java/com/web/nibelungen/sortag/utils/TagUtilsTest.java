package com.web.nibelungen.sortag.utils;

import com.web.nibelungen.sortag.TestBase;
import com.web.nibelungen.sortag.exception.SorTagException;
import org.junit.Test;

import java.util.List;

public class TagUtilsTest extends TestBase {

    @Test
    public void getTagsOfFile_Simple() throws SorTagException {
        List<String> allTagOfFolder = TagUtils.getAllTagOfFolder(this.getParametres().getExtensions(), RESSOURCE_FOLDER);

        assertThat(allTagOfFolder).isNotNull();
        assertThat(allTagOfFolder).isNotEmpty();
        assertThat(allTagOfFolder.size()).isEqualTo(2);
        assertThat(allTagOfFolder).containsExactly(FIRSTTAG, SECONDTAG);
    }

    @Test
    public void getTagsOfFile_FolderNotExist() throws SorTagException {
        List<String> allTagOfFolder = TagUtils.getAllTagOfFolder(this.getParametres().getExtensions(), "src/test/joke/");

        assertThat(allTagOfFolder).isNotNull();
        assertThat(allTagOfFolder).isEmpty();
        assertThat(allTagOfFolder.size()).isEqualTo(0);
    }

    @Test
    public void getTagsOfFile_ManyFile() throws SorTagException {
        List<String> allTagOfFolder = TagUtils.getAllTagOfFolder(this.getParametres().getExtensions(), RESSOURCE_FOLDER + "manyFiles/");

        assertThat(allTagOfFolder).isNotNull();
        assertThat(allTagOfFolder).isNotEmpty();
        assertThat(allTagOfFolder.size()).isEqualTo(3);
        assertThat(allTagOfFolder).containsExactly(FIRSTTAG, SECONDTAG, "thirdtag");
    }

    @Test
    public void getTagsOfFile_ManyFilesWithCrossedTags() throws SorTagException {
        List<String> allTagOfFolder = TagUtils.getAllTagOfFolder(this.getParametres().getExtensions(), TagUtilsTest.RESSOURCE_FOLDER + "manyFilesWithCrossedTags/");

        assertThat(allTagOfFolder).isNotNull();
        assertThat(allTagOfFolder).isNotEmpty();
        assertThat(allTagOfFolder.size()).isEqualTo(2);
        assertThat(allTagOfFolder).containsExactly(FIRSTTAG, SECONDTAG);
    }

    @Test
    public void getTagsOfFile_ManyFilesNotPicture() throws SorTagException {
        List<String> allTagOfFolder = TagUtils.getAllTagOfFolder(this.getParametres().getExtensions(), TagUtilsTest.RESSOURCE_FOLDER + "manyFilesNotPicture/");

        assertThat(allTagOfFolder).isNotNull();
        assertThat(allTagOfFolder).isEmpty();
        assertThat(allTagOfFolder.size()).isEqualTo(0);
    }

    @Test
    public void getTagsOfFile_Null() throws SorTagException {
        List<String> allTagOfFolder = TagUtils.getAllTagOfFolder(this.getParametres().getExtensions(), null);

        assertThat(allTagOfFolder).isNotNull();
        assertThat(allTagOfFolder).isEmpty();
        assertThat(allTagOfFolder.size()).isEqualTo(0);
    }
}