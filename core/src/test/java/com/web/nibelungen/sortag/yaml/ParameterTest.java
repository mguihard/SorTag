package com.web.nibelungen.sortag.yaml;

import com.web.nibelungen.sortag.TestBase;
import com.web.nibelungen.sortag.exception.SorTagException;
import org.junit.Test;

public class ParameterTest extends TestBase {

    @Test
    public void build() throws SorTagException {
        Parameter parameter = Parameter.build();

        assertThat(parameter).isNotNull();
        assertThat(parameter.getExtensions()).isNotNull();
        assertThat(parameter.getExtensions()).isNotEmpty();
        assertThat(parameter.getExtensions()).contains("jpg", "jpeg", "png");
    }
}