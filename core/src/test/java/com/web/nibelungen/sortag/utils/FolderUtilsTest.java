package com.web.nibelungen.sortag.utils;

import com.web.nibelungen.sortag.TestBase;
import org.junit.Test;

public class FolderUtilsTest extends TestBase {

    @Test
    public void countFileInFolder_Simple() {
        Long count = FolderUtils.countFileInFolder(this.getParametres().getExtensions(), SRC_TEST_RESOURCES_COUNT_FILES);

        assertThat(count).isNotNull();
        assertThat(count).isEqualTo(4L);
    }
}