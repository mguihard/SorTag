package com.web.nibelungen.sortag.utils;

import com.drew.imaging.ImageMetadataReader;
import com.drew.imaging.ImageProcessingException;
import com.drew.metadata.Directory;
import com.drew.metadata.exif.ExifIFD0Directory;
import com.drew.metadata.iptc.IptcDirectory;
import com.web.nibelungen.sortag.exception.SorTagException;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class TagUtils {

    public static List<String> getAllTagOfFolder(List<String> extension, String path) throws SorTagException {
        List<String> listTagsOfFolder = new ArrayList<>();

        if (path != null) {
            File folder = new File(path);

            if (folder.exists() && folder.listFiles() != null) {
                File[] files = folder.listFiles();

                for (File file:files) {
                    if (FolderUtils.testIfPicture(extension, file)) {
                        listTagsOfFolder.addAll(TagUtils.getTagsOfFile(extension, file));
                    }
                }
            }
        }

        return listTagsOfFolder.stream()
                .distinct()
                .sorted()
                .collect(Collectors.toList());
    }

    public static List<String> getTagsOfFile(List<String> extension, File file) throws SorTagException {
        List<String> listTagsOfFile = new ArrayList<>();

        try {
            ImageMetadataReader
                    .readMetadata(file)
                    .getDirectories()
                    .forEach(directory -> {
                        listTagsOfFile.addAll(TagUtils.getTagOfType(directory, ExifIFD0Directory.TAG_WIN_KEYWORDS));
                        listTagsOfFile.addAll(TagUtils.getTagOfType(directory, IptcDirectory.TAG_KEYWORDS));
            });
        } catch (IOException | ImageProcessingException e) {
            throw new SorTagException("2x1", e);
        }

        return listTagsOfFile;
    }

    private static List<String> getTagOfType(Directory directory, int type) {
        List<String> listTagsOfType = new ArrayList<>();

        String allDescriptions = directory.getDescription(type);
        if (StringUtils.isNoneEmpty(allDescriptions)) {
            if (allDescriptions.contains(";")) {
                Arrays.stream(allDescriptions.split(";"))
                        .distinct()
                        .collect(Collectors.toList())
                        .forEach(listTagsOfType::add);
            } else {
                listTagsOfType.add(allDescriptions);
            }
        }

        return listTagsOfType;
    }

}
