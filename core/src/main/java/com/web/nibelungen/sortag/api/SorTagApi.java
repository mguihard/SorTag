package com.web.nibelungen.sortag.api;

import com.web.nibelungen.sortag.exception.SorTagException;
import com.web.nibelungen.sortag.utils.FileUtils;
import com.web.nibelungen.sortag.utils.FolderUtils;
import com.web.nibelungen.sortag.utils.TagUtils;
import com.web.nibelungen.sortag.yaml.Parameter;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.util.List;
import java.util.Map;

public class SorTagApi {

    /**
     *
     */
    public static final String FOLDER_SEPARATE = "\\";

    /**
     *
     */
    public static final String PARAMS_REPLACER = "\\{}";

    /**
     *
     */
    private Parameter parameter;

    /**
     * @param parameter
     */
    public SorTagApi(Parameter parameter) {
        this.parameter = parameter;
    }

    /**
     *
     */
    public SorTagApi() {
        try {
            this.parameter = Parameter.build();
        } catch (SorTagException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param path
     * @return
     */
    public List<File> folderApiListFiles(String path) {
        return FolderUtils.fileOfFolder(parameter.getExtensions(), path);
    }

    /**
     * @param path
     * @return
     */
    public Long folderApiCountFile(String path) {
        return FolderUtils.countFileInFolder(parameter.getExtensions(), path);
    }

    /**
     * @param sourceFile
     * @param destinationFile
     * @throws SorTagException
     */
    public void fileApiCopyFile(File sourceFile, File destinationFile) throws SorTagException {
        FileUtils.copyFile(sourceFile, destinationFile);
    }

    /**
     * @param file
     * @return
     * @throws SorTagException
     */
    public List<String> tagApiOfFile(File file) throws SorTagException {
        return TagUtils.getTagsOfFile(parameter.getExtensions(), file);
    }

    /**
     * @param path
     * @return
     * @throws SorTagException
     */
    public List<String> tagApiOfFolder(String path) throws SorTagException {
        return TagUtils.getAllTagOfFolder(parameter.getExtensions(), path);
    }

    /**
     * @param code
     * @param value
     * @return
     * @throws SorTagException
     */
    public String getErrorMessage(String code, String... value) throws SorTagException {
        Map<String, String> error = this.parameter.getError();
        String message;

        if (!StringUtils.isEmpty(error.get(code))) {
            message = error.get(code);
        } else {
            throw new SorTagException("3x1");
        }


        if (value != null) {
            for (String str:value) {
                if (message.contains("{}")) {
                    message = message.replaceFirst(SorTagApi.PARAMS_REPLACER, str);
                }
            }
        }

        return message;
    }

    /**
     * @return
     */
    public Map<String, String> sysApiGetParams() {
        return this.parameter.getParams();
    }
}
