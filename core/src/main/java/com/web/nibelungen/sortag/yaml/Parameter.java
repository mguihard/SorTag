package com.web.nibelungen.sortag.yaml;

import com.web.nibelungen.sortag.exception.SorTagException;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Map;

@Getter
@Setter
public class Parameter extends YamlBase {
    /**
     * URL
     */
    private static final String YAML_NAME = "app.yml";

    /**
     * Ectension gerer
     */
    private List<String> extensions;

    /**
     * ERROR
     */
    private Map<String, String> error;

    /**
     * ERROR
     */
    private Map<String, String> params;

    /**
     * @return Parameter almienter avec le yaml
     */
    public static Parameter build() throws SorTagException {
        return YamlBase.build(YAML_NAME, Parameter.class);
    }
}
