package com.web.nibelungen.sortag.utils;

import org.apache.commons.collections4.CollectionUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FolderUtils {

    public static Long countFileInFolder(List<String> extension, String path) {
        Long count = 0L;

        List<File> nbFile = FolderUtils.fileOfFolder(extension, path);

        if (!CollectionUtils.isEmpty(nbFile)) {
            count = Long.valueOf(nbFile.size());
        }

        return count;
    }

    public static List<File> fileOfFolder(List<String> extension, String path) {
        File folder = new File(path);
        List<File> files = null;

        if (folder.exists()) {
            files = new ArrayList<>();

            Arrays.stream(folder.listFiles())
                    .filter(pic -> FolderUtils.testIfPicture(extension, pic))
                    .forEach(files::add);
        }

        return files;
    }

    public static Boolean testIfPicture(List<String> extension, File file) {

        return extension.stream()
                .anyMatch(ext -> file.getName().endsWith(ext));
    }
}
