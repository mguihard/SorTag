package com.web.nibelungen.sortag.yaml;

import com.web.nibelungen.sortag.enums.Languages;
import com.web.nibelungen.sortag.exception.SorTagException;
import lombok.Getter;
import lombok.Setter;

import java.util.Map;

@Getter
@Setter
public class Messages extends YamlBase {
    /**
     * URL
     */
    private static final String SRC = "messages_{}.yml";

    /**
     * URL
     */
    private static final String SRC_DEFAULT = "messages_fr.yml";

    /**
     * Labels
     */
    private Map<String, String> labels;

    /**
     * Logs
     */
    private Map<String, String> logs;

    /**
     * Languages
     */
    private Map<String, String> lang;

    /**
     * @return Parameter almienter avec le yaml
     */
    public static Messages build(Languages language) throws SorTagException {
        return Messages.loadMessage(language.getCode(), SRC, SRC_DEFAULT);
    }

    public String getLabel(String key, String... params) {
        return this.get(this.labels, key, params);
    }

    public String getLog(String key, String... params) {
        return this.get(this.logs, key, params);
    }

    public String getLang(String key, String... params) {
        return this.get(this.lang, key, params);
    }
}
