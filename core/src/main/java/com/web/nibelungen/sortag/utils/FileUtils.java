/**
 * Classe utilitaire pour les fichiers
 */
package com.web.nibelungen.sortag.utils;

import com.web.nibelungen.sortag.exception.SorTagException;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

/**
 * @author Mickaël
 *
 */
public class FileUtils {

	/**
	 * Copy un fichier vers un repertoire
	 *
	 * @param sourceFile Source File
	 * @param destinationFile Destination File
	 * @throws SorTagException Exception
	 */
	public static void copyFile(File sourceFile, File destinationFile) throws SorTagException {
		Path source = Paths.get(sourceFile.getPath());
		Path destination = Paths.get(destinationFile.getPath());

		try {
			Files.copy(source , destination, StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e) {
			throw new SorTagException("1x0", e);
		}
	}
}
