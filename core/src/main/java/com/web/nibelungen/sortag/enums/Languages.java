package com.web.nibelungen.sortag.enums;

import lombok.Getter;
import lombok.Setter;


public enum Languages {
    FR("fr"), EN("en");

    @Getter
    @Setter
    private String code;

    Languages(String code) {
        this.code = code;
    }
}
