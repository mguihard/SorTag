package com.web.nibelungen.sortag.yaml;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.web.nibelungen.sortag.exception.SorTagException;

import java.io.File;
import java.io.IOException;
import java.util.Map;

public class YamlBase {

    protected static <T extends YamlBase> T build(String path, Class toClass) throws SorTagException {
        T toObject;
        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());

        try {
            toObject = (T) mapper.readValue(ClassLoader.getSystemClassLoader().getResource(path), toClass);
        } catch (IOException e) {
            throw new SorTagException("0x0", e);
        }

        return toObject;
    }

    protected static <T extends YamlBase> T loadMessage(String language, String i18nFile, String defaultFile) throws SorTagException {
        String path;
        T messages;
        File file;

        path = i18nFile.replaceFirst("\\{}", language);
        String str = ClassLoader.getSystemClassLoader().getResource(path).getPath();
        file = new File(str);

        if(file.exists()) {
            messages = YamlBase.build(path, Messages.class);
        } else {
            messages = YamlBase.build(defaultFile, Messages.class);
        }

        return messages;
    }

    protected String get(Map<String, String> val, String key, String... params) {
        String msg = val.get(key);

        if(params != null && params.length > 0) {
            for (String param:params) {
                if (param.contains("\\")) {
                    param = param.replace("\\", "\\\\");
                }

                if (msg.contains("{}")) {
                    msg = msg.replaceFirst("\\{}", param);
                }
            }
        }

        return msg;
    }
}
