package com.web.nibelungen.sortag.exception;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SorTagException extends Exception {

    /**
     *
     */
    private String code;

    /**
     *
     */
    private Exception origineException;

    /**
     * Constructor
     *
     * @param code String
     */
    public SorTagException(String code, Exception e) {
        this.code = code;
        this.origineException = e;
    }

    /**
     * Constructor
     *
     * @param code String
     */
    public SorTagException(String code) {
        this.code = code;
    }
}
