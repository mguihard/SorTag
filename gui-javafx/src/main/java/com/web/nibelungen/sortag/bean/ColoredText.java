package com.web.nibelungen.sortag.bean;

import javafx.scene.paint.Color;

public class ColoredText {
    /**
     *
     */
    private final String text;

    /**
     *
     */
    private final Color color;

    /**
     * @param text
     * @param color
     */
    public ColoredText(String text, Color color) {
        this.text = text;
        this.color = color;
    }

    /**
     * @return
     */
    public String getText() {
        return text;
    }

    /**
     * @return
     */
    public Color getColor() {
        return color;
    }
}
