package com.web.nibelungen.sortag;

import com.web.nibelungen.sortag.constantes.Constantes;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class SorTagJavaFx extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root;
        Scene scene;

        root = FXMLLoader.load(SorTagJavaFx.class.getResource(Constantes.VIEW_SORTAG));
        scene = new Scene(root, Constantes.VIEW_SIZE[0], Constantes.VIEW_SIZE[1]);

        primaryStage.setTitle("SorTag v0.1");
        primaryStage.setScene(scene);
        primaryStage.setResizable(false);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
