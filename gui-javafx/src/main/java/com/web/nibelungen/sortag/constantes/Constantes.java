package com.web.nibelungen.sortag.constantes;

public class Constantes {

    public static final String VIEW_BASE = "view/";
    public static final String VIEW_SORTAG = VIEW_BASE + "sorTag.fxml";
    public static final Double[] VIEW_SIZE = {710.0, 290.0};
}
