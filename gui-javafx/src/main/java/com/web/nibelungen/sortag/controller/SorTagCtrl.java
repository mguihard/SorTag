package com.web.nibelungen.sortag.controller;

import com.web.nibelungen.sortag.api.SorTagApi;
import com.web.nibelungen.sortag.bean.ColoredText;
import com.web.nibelungen.sortag.constantes.LogType;
import com.web.nibelungen.sortag.enums.Languages;
import com.web.nibelungen.sortag.exception.SorTagException;
import com.web.nibelungen.sortag.yaml.Messages;
import javafx.application.Application;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.FlowPane;
import javafx.scene.paint.Color;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import org.apache.commons.collections4.CollectionUtils;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class SorTagCtrl extends Application {

    private SorTagApi sorTagApi;

    private Messages messages;

    private Stage primaryStage;

    private Map<String, String> params;

    @FXML
    private Tab tabParams;

    @FXML
    private Tab tabLog;

    @FXML
    private Button btnDel;

    @FXML
    private Button btnSort;

    @FXML
    private TextField txtSource;

    @FXML
    private Button btnSource;

    @FXML
    private Label lblNb;

    @FXML
    private TextField txtNa;

    @FXML
    private Button btnRefresh;

    @FXML
    private TextField txtDest;

    @FXML
    private Button btnDest;

    @FXML
    private FlowPane flowTags;

    @FXML
    private FlowPane flowNbFile;

    @FXML
    private FlowPane flowDest;

    @FXML
    private ListView<String> txtTags;

    @FXML
    private ProgressBar progressBar;

    @FXML
    private CheckBox checkNoTag;

    @FXML
    private MenuItem mniSource;

    @FXML
    private MenuItem mniDest;

    @FXML
    private Menu mnFile;

    @FXML
    private Menu mnLocal;

    @FXML
    private ListView<ColoredText> listLogs;

    @FXML
    private void initialize() {
        this.init(Languages.FR);
        this.initLog();
        this.addLogs(LogType.NORMAL, this.messages.getLog("start.app"));
    }

    private void initLog() {
        this.listLogs.setCellFactory(lv -> new ListCell<ColoredText>() {
            @Override
            protected void updateItem(ColoredText item, boolean empty) {
                super.updateItem(item, empty);
                if (item == null) {
                    setText(null);
                    setTextFill(null);
                } else {
                    setText(item.getText());
                    setTextFill(item.getColor());
                }
            }
        });
    }

    @Override
    public void start(final Stage primaryStage) {
        this.primaryStage = primaryStage;
    }

    private void init(Languages languages) {
        try {
            this.sorTagApi = new SorTagApi();
            this.initLabels(languages);
            this.params = this.sorTagApi.sysApiGetParams();

            if (this.params == null || this.params.isEmpty()) {
                this.addLogs(LogType.ERROR, this.messages.getLog("error.params"));
            }

        } catch (SorTagException e) {
            this.addLogs(LogType.ERROR, this.messages.getLog("error.init"));
        }
    }

    private void addLogs(LogType logType, String log) {
        SimpleDateFormat dt = new SimpleDateFormat("[dd/mm/yyyy hh:mm:ss] ");
        String baseTxt = dt.format(new Date());
        ColoredText text;

        switch (logType){
            case SUCESS:
                text = new ColoredText(baseTxt + log, Color.GREEN);
                break;
            case WARNING:
                text = new ColoredText(baseTxt + log, Color.ORANGE);
                break;
            case ERROR:
                text = new ColoredText(baseTxt + log, Color.RED);
                break;
            default:
            case NORMAL:
                text = new ColoredText(baseTxt + log, Color.DARKGRAY);
                break;
        }

        this.listLogs.getItems().add(text);
    }

    private void initLabels(Languages languages) throws SorTagException {
        this.messages = Messages.build(languages);

        this.tabParams.setText(messages.getLabels().get("tab.param"));
        this.tabLog.setText(messages.getLabels().get("tab.log"));
        this.btnDel.setText(messages.getLabels().get("btn.del"));
        this.btnSort.setText(messages.getLabels().get("btn.sort"));
        this.txtSource.setText(this.messages.getLabels().get("txt.source"));
        this.btnSource.setText(this.messages.getLabels().get("btn.source"));
        this.lblNb.setText(this.messages.getLabels().get("lbl.nb"));
        this.txtNa.setText(this.messages.getLabels().get("lbl.n"));
        this.btnRefresh.setText(this.messages.getLabels().get("btn.refresh"));
        this.txtDest.setText(this.messages.getLabels().get("txt.dest"));
        this.btnDest.setText(this.messages.getLabels().get("btn.dest"));
        this.mnFile.setText(this.messages.getLabels().get("mn.file"));
        this.mniSource.setText(this.messages.getLabels().get("mni.source"));
        this.mniDest.setText(this.messages.getLabels().get("mni.dest"));
        this.mnLocal.setText(this.messages.getLabels().get("mn.locale"));
        this.checkNoTag.setText(this.messages.getLabels().get("ck.no.tag"));
        this.mnLocal.getItems()
                .remove(0, this.mnLocal.getItems().size());

        for (Languages language:Languages.values()) {
            MenuItem menu = new MenuItem();
            menu.setText(this.messages.getLang(language.getCode()));
            menu.setUserData(language.getCode());
            menu.setOnAction((ActionEvent event) -> {
                this.onChangeLanguage(event);
            });

            this.mnLocal.getItems().add(menu);
        }

        this.addLogs(LogType.NORMAL, this.messages.getLog("load.lang", languages.getCode().toUpperCase()));
    }

    private String openDirectory(TextField textField) {
        DirectoryChooser directoryChooser = new DirectoryChooser();
        File selectedDirectory = directoryChooser.showDialog(this.primaryStage);

        if(selectedDirectory == null){
            this.addLogs(LogType.WARNING, this.messages.getLog("folder.none.src"));
        }else{
            textField.setText(selectedDirectory.getAbsolutePath());
        }

        this.addLogs(LogType.SUCESS, this.messages.getLog("folder.comp.src", textField.getText()));
        return textField.getText();
    }

    private void refreshNbFile(String path) {
        Long count = this.sorTagApi.folderApiCountFile(path);
        this.txtNa.setText(String.valueOf(count));
        this.addLogs(LogType.SUCESS, this.messages.getLog("tag.nb", String.valueOf(count)));
    }

    private void refreshTags(String path) {
        try {
            this.txtTags.getItems().clear();
            List<String> lTags = this.sorTagApi.tagApiOfFolder(path);
            ObservableList<String> items = this.txtTags.getItems();

            if (!CollectionUtils.isEmpty(lTags)) {
                this.flowTags.setDisable(false);
                lTags.forEach(items::add);
            }

        } catch (SorTagException e) {
            this.addLogs(LogType.ERROR, this.messages.getLog("error.rf.tag"));
        }
    }

    private Boolean hideShow(String path) {
        File file = new File(path);
        Boolean exist = false;

        if (file.exists()) {
            this.flowNbFile.setDisable(false);
            this.flowDest.setDisable(false);
            this.btnSort.setDisable(false);
            this.mniDest.setDisable(false);
            exist = true;
        } else {
            this.flowNbFile.setDisable(true);
            this.flowDest.setDisable(true);
            this.btnSort.setDisable(true);
            this.mniDest.setDisable(true);
        }

        return exist;
    }

    private void processForPicture(File item) throws SorTagException {
        List<String> lTags = this.sorTagApi.tagApiOfFile(item);
        String noTag = this.params.get("no.tag");

        ArrayList<String> items = new ArrayList<>();
        items.addAll(this.txtTags.getItems());

        if (this.checkNoTag.isSelected()) {
            items.add(noTag);
        }

        if (CollectionUtils.isEmpty(lTags)) {
            this.addLogs(LogType.NORMAL, this.messages.getLog("tags", item.getName(), String.valueOf(lTags.size())));
            lTags = new ArrayList<>();
            lTags.add(noTag);
        } else {
            this.addLogs(LogType.NORMAL, this.messages.getLog("tags.empty", item.getName()));
        }

        lTags.stream()
                .filter(items::contains)
                .forEach(tag -> this.processForTag(item, tag));
    }

    private void processForTag(File item, String tag) {
        StringBuilder pathDirectory;
        StringBuilder pathPicture;
        File folderDest;

        pathDirectory = new StringBuilder();
        pathDirectory.append(this.txtDest.getText());
        pathDirectory.append(SorTagApi.FOLDER_SEPARATE);
        pathDirectory.append(tag);

        folderDest = new File(pathDirectory.toString());

        if (!folderDest.exists()) {
            this.addLogs(LogType.NORMAL, this.messages.getLog("folder", folderDest.getName()));
            if(folderDest.mkdir()) {
                this.addLogs(LogType.SUCESS, this.messages.getLog("folder.success", folderDest.getName()));
            } else {
                this.addLogs(LogType.ERROR, this.messages.getLog("folder.fail", folderDest.getName()));
            }
        }

        try {
            pathPicture = new StringBuilder();
            pathPicture.append(folderDest.getPath());
            pathPicture.append(SorTagApi.FOLDER_SEPARATE);
            pathPicture.append(item.getName());

            this.addLogs(LogType.NORMAL, this.messages.getLog("move", item.getName(), folderDest.getPath()));
            this.sorTagApi.fileApiCopyFile(item, new File (pathPicture.toString()));
            this.addLogs(LogType.SUCESS, this.messages.getLog("move.sucess", item.getName(), folderDest.getPath()));
        } catch (SorTagException e) {
            this.addLogs(LogType.ERROR, this.messages.getLog("move.fail", item.getName(), folderDest.getPath()));
        }
    }

    private void incrementeProgresBar() {
        String nbFileTxt = this.txtNa.getText();
        Double nbFileLong = Double.valueOf(nbFileTxt);

        Double resultat = this.progressBar.getProgress() + 1 / nbFileLong;
        this.progressBar.setProgress(resultat);
    }

    /**********************************
     * EVENTY
     **********************************/

    @FXML
    private void onChangeLanguage(ActionEvent event) {
        try {
            String str = ((MenuItem) event.getSource()).getUserData().toString();
            Languages languages = Languages.valueOf(str.toUpperCase());

            this.initLabels(languages);
        } catch (SorTagException e) {
            this.addLogs(LogType.ERROR, this.messages.getLog("error.lang"));
        }
    }

    @FXML
    public void onClickBtnSource() {
        String folder = this.openDirectory(this.txtSource);
        StringBuilder label = new StringBuilder();

        if (this.hideShow(folder)) {
            label.append(this.txtSource.getText());
            label.append(SorTagApi.FOLDER_SEPARATE);
            label.append(this.params.get("folder.name"));

            this.txtDest.setText(label.toString());
            this.refreshNbFile(folder);
            this.refreshTags(folder);
        }
    }

    @FXML
    public void onClickBtnDest() {
        this.openDirectory(this.txtDest);
    }

    @FXML
    public void onClickBtnRefresh() {
        this.refreshNbFile(this.txtSource.getText());
        this.refreshTags(this.txtSource.getText());
    }

    public void onClickDelTag() {
        MultipleSelectionModel<String> selectionModel = this.txtTags.getSelectionModel();

        if (selectionModel != null) {
            this.txtTags.getItems().removeAll(selectionModel.getSelectedItems());
        }
    }

    @FXML
    public void onClickBtnSort() {
        this.addLogs(LogType.SUCESS, this.messages.getLog("start"));
        File baseFolder = new File(this.txtDest.getText());
        this.progressBar.setProgress(0);
        List<File> lFiles = null;
        List<String> lTags = null;

        if (!baseFolder.exists()) {
            baseFolder.mkdir();
        }

        try {
            lFiles = this.sorTagApi.folderApiListFiles(this.txtSource.getText());
            lTags = this.sorTagApi.tagApiOfFolder(this.txtSource.getText());
        } catch (SorTagException e) {
            e.printStackTrace();
        }

        this.addLogs(LogType.SUCESS, this.messages.getLog("init", String.valueOf(lFiles.size()), String.valueOf(lTags.size()), this.txtDest.getText()));

        lFiles.stream()
                .forEach(file -> {
            try {
                this.processForPicture(file);
            } catch (SorTagException e) {
                this.addLogs(LogType.ERROR, this.messages.getLog("tags.error", file.getName()));
            } finally {
                this.incrementeProgresBar();
            }
        });

        this.addLogs(LogType.SUCESS, this.messages.getLog("stop"));
    }


}
